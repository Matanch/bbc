import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from './interfaces/user';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
 
  user:Observable<User | null> //null is user not login

  constructor(public afAuth:AngularFireAuth, private router:Router) { 
    this.user = this.afAuth.authState;
  }

  getUser(){
    return this.user
  }
  signup(email:string, password:string){
    this.afAuth
        .auth
        .createUserWithEmailAndPassword(email,password)
        .then(user =>{this.router.navigate(['/books']);
    })
  }
  Logout(){
    this.afAuth.auth.signOut()
    .then(res => console.log('logout',res)
    )
  }

  login(email:string, password:string){
    this.afAuth
        .auth.signInWithEmailAndPassword(email,password)
        .then( user =>{this.router.navigate(['/books']);
  })
  }
}