import { AngularFirestoreModule, AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  books:any = [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'},{title:'The Magic Mountain', author:'Thomas Mann'},{title:'TOY STORY', author:'Matan Chover'}]
/*
 getBooks(){
  const bookObservable = new Observable(
    observer => {
      setInterval(
       () =>observer.next(this.books),5000)
    })
    
  return bookObservable;
 }

 */

userCollection:AngularFirestoreCollection = this.db.collection('users');
bookCollection:AngularFirestoreCollection;

getBooks(userId): Observable<any[]> {
  //const ref = this.db.collection('books');
  //return ref.valueChanges({idField: 'id'});
  this.bookCollection = this.db.collection(`users/${userId}/books`);
  console.log('Books collection created');
  return this.bookCollection.snapshotChanges().pipe(
    map(collection => collection.map(document => {
      const data = document.payload.doc.data();
      data.id = document.payload.doc.id;
      console.log(data);
      return data;
    }))
  );    
} 

getBook(userId, id:string):Observable<any>{
  return this.db.doc(`users/${userId}/books/${id}`).get();
}

addBook(userId:string, title:string, author:string){
  console.log('In add books');
  const book = {title:title,author:author}
  //this.db.collection('books').add(book)  
  this.userCollection.doc(userId).collection('books').add(book);
} 

updateBook(userId:string, id:string,title:string,author:string){
  this.db.doc(`users/${userId}/books/${id}`).update(
    {
      title:title,
      author:author
    }
  )
}

deleteBook(userId:string, id:string){
  this.db.doc(`users/${userId}/books/${id}`).delete();
}
 /*addBooks(){
   setInterval(
    ()=> this.books.push({title:'A new book', author:'New author'}),5000)
 }
 */
  // getBooks(){
  //   setInterval(()=>this.books,1000)
  // }
  
  constructor(private db: AngularFirestore,
    private authService:AuthService) {}
}
