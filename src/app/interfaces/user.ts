export interface User {
    //?  - field in not required
    uid:string,
    email?: string | null,
    photoUrl?: string,
    displayName?:string
 
}
